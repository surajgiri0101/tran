import { ElementFinder,element,by } from "protractor";


export class angularHomePage
{

    angularLink:ElementFinder;
    search:ElementFinder;

    constructor()
    {
        this.angularLink=element(by.css("a[href='https://blog.angular.io/finding-a-path-forward-with-angularjs-7e186fdd4429']"));
        this.search=element(by.css("input[type='search']"));
    }

}