"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.angularHomePage = void 0;
const protractor_1 = require("protractor");
class angularHomePage {
    constructor() {
        this.angularLink = (0, protractor_1.element)(protractor_1.by.css("a[href='https://blog.angular.io/finding-a-path-forward-with-angularjs-7e186fdd4429']"));
        this.search = (0, protractor_1.element)(protractor_1.by.css("input[type='search']"));
    }
}
exports.angularHomePage = angularHomePage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhckhvbWVQYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZU9iamVjdC9hbmd1bGFySG9tZVBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsMkNBQXNEO0FBR3RELE1BQWEsZUFBZTtJQU14QjtRQUVJLElBQUksQ0FBQyxXQUFXLEdBQUMsSUFBQSxvQkFBTyxFQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsc0ZBQXNGLENBQUMsQ0FBQyxDQUFDO1FBQ3pILElBQUksQ0FBQyxNQUFNLEdBQUMsSUFBQSxvQkFBTyxFQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO0lBQ3hELENBQUM7Q0FFSjtBQVpELDBDQVlDIn0=